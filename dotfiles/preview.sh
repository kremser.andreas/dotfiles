#!/bin/bash

REVERSE="\x1b[7m"
RESET="\x1b[m"

if [ "$1" == "-v" ]; then
  SPLIT=1
  shift
fi

if [ -z "$1" ]; then
  echo "usage: $0 [-v] FILENAME[:LINENO][:IGNORED]"
  exit 1
fi

IFS=':' read -r -a INPUT <<< "$1"
FILE=${INPUT[0]}
CENTER=${INPUT[1]}

if [[ $1 =~ ^[A-Z]:\\ ]]; then
  FILE=$FILE:${INPUT[1]}
  CENTER=${INPUT[2]}
fi

if [[ -d "$FILE" ]]; then
  if [ -x "$(command -v exa)" ]; then
    exa --color=always -L 1 --tree "$FILE" | head -58
  else
    ls -l --color=always "$FILE" | head -58
  fi
  exit 0
fi

if [ ! -r "$FILE" ]; then
  echo "File not found ${FILE}"
  exit 1
fi

if [ -z "$CENTER" ]; then
  CENTER=1
fi

if [ -z "$LINES" ]; then
  if [ -r /dev/tty ]; then
    LINES=$(stty size < /dev/tty | awk '{print $1}')
  else
    LINES=40
  fi
  if [ -n "$SPLIT" ]; then
    LINES=$(($LINES/2)) # using horizontal split
  fi
  LINES=$(($LINES-2)) # remove preview border
fi

FIRST=$(($CENTER-$LINES/3))
FIRST=$(($FIRST < 1 ? 1 : $FIRST))
LAST=$((${FIRST}+${LINES}-1))

if [[ "$(file --mime "$FILE")" =~ binary ]]; then
  echo "<BINARY FILE>: $(file "$FILE")"
  if [ ! -z "$(find "$FILE" -type f)" ]; then hexdump -C "$FILE" | head -n $LAST | tail -n +$FIRST; fi
  exit 0
fi


if [ -x "$(command -v bat)" ] ; then
  bat --color always --style numbers,changes,grid --terminal-width=$(tput cols) --line-range $FIRST:$LAST "$FILE" -H $CENTER
else
  cat -n "$FILE" | head -n $LAST | tail -n +$FIRST
fi

#todo : checkout http://blog.z3bra.org/2014/01/images-in-terminal.html for w3mimgdisplay
