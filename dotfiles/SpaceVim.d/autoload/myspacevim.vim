function! myspacevim#before() abort
    let g:startify_custom_header = []
    let g:neomake_enabled_c_makers = ['clang']
    let g:spacevim_snippet_engine = 'ultisnips'
    let g:ShowMultiBase_Display_Decimal_Show = 1
    nnoremap jk <Esc>
    nnoremap jj <Esc>
    " +=unnamedplus
    " g:SimplyFold_docstring_preview=1
    " https://qifei9.github.io/2018/spacevim-give-up.html
    set clipboard=unnamedplus
    set foldmethod=syntax
    set nofoldenable
    set autochdir
    set smartcase
    set ignorecase
    set noundofile
    
    "autocd into current buffers file
    autocmd BufEnter * silent! lcd %:p:h
    autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
    "preview results of commands in split window 
    set inccommand=split
    " nnoremap <silent> <Leader>fl :Locate<Space>
    call SpaceVim#custom#SPC('nnoremap', ['f', 'l' ] , ':Locate ', "search files with locate", 0)
    nmap gx yiW:!xdg-open <cWORD><CR> <C-r>" & <CR>><CR>
endfunction

function! myspacevim#after() abort
    iunmap jk
    iunmap jj
    set noundofile
endfunction

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

command! -nargs=1 -bang Locate call fzf#run(fzf#wrap( {'source': 'locate <q-args>', 'options': '-m'}, <bang>0))

command Hexedit :Vinarise
