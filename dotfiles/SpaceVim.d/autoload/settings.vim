set clipboard=unnamedplus
set foldmethod=manual
set nofoldenable
set autochdir
set ignorecase
autocmd BufEnter * silent! lcd %:p:h

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


