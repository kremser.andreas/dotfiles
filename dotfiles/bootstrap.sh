#!/usr/bin/env bash
#stop on error
set -e
#show all executed commands
set -x
#update package repository
sudo apt update
#install downloader
sudo apt install curl wget
sudo apt install gcc

#copy terminal output to clipboard
sudo apt install xsel xclip
#suggest terminal command correction
sudo apt install python-setuptools thefuck
#recently used files/directories for terminal
sudo apt install fasd
#breadth first find
sudo apt install bfs
#suggest installation candidates for commands
sudo apt install command-not-found
#quickly locate files
sudo apt install locate
#colorize shell commands
sudo apt install grc
#better shell
sudo apt install zsh
#terminal multiplexer
sudo apt install tmux

# #install common development libraries+tools
# sudo apt install autoconf automake bc binutils-dev bison build-essential cmake \
#      ctags global curl fakeroot flex g++-multilib gcc-multilib git gnupg lib32ncurses5-dev \
#      lib32z1-dev libc6-dev-i386 libcairo2-dev libelf-dev libevent-dev libgif-dev \
#      libgif7 libgit2-dev libgl1-mesa-dev libgnutls28-dev libgtk-3-dev libjpeg-dev \
#      libm17n-0 libm17n-dev libmagic-dev libmagick++-dev libmagickwand-dev libncurses5 \
#      libncurses5-dev libncursesw5-dev libotf-dev libotf0 libpng-dev librsvg2-dev \
#      libssl-dev libtiff5-dev libwebkit2gtk-4.0-dev libx11-dev libxml2-dev \
#      libxml2-utils libxpm-dev lzop make openssl python3 python3-pip subversion \
#      texinfo unzip x11proto-core-dev xz-utils yodl zip zlib1g-dev clang clang-tools \
#      clang-format python3 python3-sphinx pigz gdb

#install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
export PATH="$HOME/.cargo/bin:$PATH"
#better grep
cargo install -f ripgrep
#better cat
cargo install -f bat
#better ls
cargo install -f exa

# #gui text editor
# sudo apt install geany
# #vs-code
# sudo snap install --classic code

# #install coding fonts
sudo apt install fonts-hack
# sudo apt install fonts-hack fonts-firacode fonts-inconsolata
# FONT_HOME=~/.local/share/fonts
# mkdir -p "$FONT_HOME/adobe-fonts/source-code-pro"
# git checkout 2.030R-ro/1.050R-it -C "$FONT_HOME/adobe-fonts/source-code-pro"
# fc-cache -f -v "$FONT_HOME/adobe-fonts/source-code-pro"

#command line text editor vim
sudo apt install vim-gtk
curl -sLf https://spacevim.org/install.sh | bash
cp -rTf ./SpaceVim.d/ ~/.SpaceVim.d

#configure fasd
cp ./fasdrc ~/.fasdrc
#configure zsh
cp ./zshrc ~/.zshrc
#configure dircolors
cp ./lscolors ~/.lscolors
#configure tmux
cp ./tmux.conf ~/.tmux.conf

#terminal-file-preview
mkdir -p ~/.local/bin
cp ./preview.sh ~/.local/bin/preview.sh

#add fzf and cargo to path if available
if ! grep -q -e 'export.*fzf' ~/.profile ; then
cat << 'EOF' >> ~/.profile
if [ -d "$HOME/.fzf/bin" ] ; then
  export PATH="$HOME/.fzf/bin:$PATH"
fi
EOF
fi

if ! grep -q -e 'export.*cargo' ~/.profile ; then
cat << 'EOF' >> ~/.profile
if [ -d "$HOME/.cargo/bin" ] ; then
  export PATH="$HOME/.cargo/bin:$PATH"
fi
EOF
fi

if ! grep -q -e 'export.*.local/bin' ~/.profile ; then
cat << 'EOF' >> ~/.profile
if [ -d "$HOME/.local/bin" ] ; then
  export PATH="$HOME/.local/bin:$PATH"
fi
EOF
fi
